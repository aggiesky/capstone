package com.vadenent;

public class FormPublicMovieFilter {
	
	private Long	movieId;

	
	public Long getMovieId() {
		return movieId;
	}

	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}

	
	@Override
	public String toString() {
		return "FormPublicMovieFilter [movieId=" + movieId + "]";
	}
	

}
