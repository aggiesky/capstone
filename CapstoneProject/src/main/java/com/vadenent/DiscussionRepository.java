package com.vadenent;

import java.util.List;

import org.springframework.data.repository.CrudRepository;



public interface DiscussionRepository extends CrudRepository<Discussion, Long> {

	List<Discussion> findAllByIdAndMovieId(Long discussionId, Long movieId);
	Discussion findByMovieIdAndWatchgroupId(Long movieId, Long watchgroupId);
	List<Discussion> findAllByPublicDiscussion(boolean publicDiscussion);
	List<Discussion> findAllByWatchgroupNameIn(List<String> watchgroupName);
	
}
