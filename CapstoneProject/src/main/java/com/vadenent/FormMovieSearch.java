package com.vadenent;

public class FormMovieSearch {
	
	private String searchString;

	
	
	public String getSearchString() {
		return searchString;
	}
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	
	@Override
	public String toString() {
		return "FormMovieSearch [searchString=" + searchString + "]";
	}
	

}
