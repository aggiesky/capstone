package com.vadenent;


public class FormCreateDiscussion {
	
	private String movieTitle;
	private Long   movieId;
	private String watchgroupName;
	private Long   watchgroupId;
	private String commentTitle;
	private String commentText;
	
	
	public String getMovieTitle() {
		return movieTitle;
	}
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	public Long getMovieId() {
		return movieId;
	}
	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	public String getWatchgroupName() {
		return watchgroupName;
	}
	public void setWatchgroupName(String watchgroupName) {
		this.watchgroupName = watchgroupName;
	}
	public Long getWatchgroupId() {
		return watchgroupId;
	}
	public void setWatchgroupId(Long watchgroupId) {
		this.watchgroupId = watchgroupId;
	}
	public String getCommentTitle() {
		return commentTitle;
	}
	public void setCommentTitle(String commentTitle) {
		this.commentTitle = commentTitle;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	
	
	@Override
	public String toString() {
		return "FormCreateDiscussion [movieTitle=" + movieTitle + ", movieId=" + movieId + ", watchgroupName="
				+ watchgroupName + ", watchgroupId=" + watchgroupId + ", commentTitle=" + commentTitle
				+ ", commentText=" + commentText + "]";
	}

	
}
