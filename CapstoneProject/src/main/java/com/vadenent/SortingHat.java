package com.vadenent;

import java.util.HashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SortingHat {
	
	
	@Autowired
	DiscussionRepository discussionRepository;
	
	@Autowired
	SessionState sessionState;
	
	
	
	
	// Calculate for the caller the new locationKey based on the input locationKey (parentSortingTag) 
	// and the sortingTagMap for a given discussion.
	
	public String getNewSortingTag(String parentSortingTag, HashMap<String, Integer> sortingTagMap) {
		
				
		// latestTagLevel structured as one field after another; each field is a string number; fields separated by ":"
		//
		// get length of latestTagLevel
		// get length of parentTagLevel
		//
		// if = , then append "01" and return;
		// if !=, increment the next field within the latestTagLevel and return that field appended to parentTagLevel
		//        also update field within latestTagLevel without impacting subsequent fields.
		//
		// e.g.  parentTagLevel == "01:07"
		//       latestTagLevel == "01:09:03:01"
		//       newCommentTagLevel == "01:07:04" -- new Comment is a child of the parent tag with next field incremented
		
		String newCommentTagLevel;
		
		// get the nextSortingTagField from the map; increment by 1; save the new value back in the map;
		//   use the original value to build the newCommentSortingTag
		
		
		int nextFieldValue = sortingTagMap.get(parentSortingTag);
		sortingTagMap.put(parentSortingTag, nextFieldValue - 1);			// value saved in map has been incremented
						
		newCommentTagLevel = String.format("%s:%03d", parentSortingTag, nextFieldValue);
		sortingTagMap.put(newCommentTagLevel, 999);
				
		return newCommentTagLevel;
	}

	
	
	// The currently active Discussions are saved in the SessionState object.  This method
	// retrieves the discussion object and returns it to the caller.
	
	Discussion getDiscussionObject(String viewName) {
		
		// set the discussionId based on the requesting page
		Long discussionId = 0L;
		
		if (viewName.equals("main")) 
			discussionId = sessionState.getHomeViewDiscussionId();
		else
			discussionId = sessionState.getDiscussionViewDiscussionId();
		
		
		// Get the current discussion
		Discussion discussion = new Discussion();
		Optional<Discussion> optionalDiscussion = discussionRepository.findById(discussionId);

		if (optionalDiscussion.isPresent())  discussion = optionalDiscussion.get();

		return discussion;
	}
	
	
	
	// Called everytime the Comments for a given discussion are passed to a view so that 
	// the view can use a variable indentation that helps the User know which comments are
	// children to which parent comments.
	
	int getIndentSize(String locationKey) {
		int indentSize = (locationKey.length() - 15) / 4;
		return indentSize;
	}

	
	
	// Call this once when a discussion is created to setup the SortingTagMap object which will
	// be used to hold all of the locationKeys for that discussion.
	
	HashMap<String, Integer> createSortingTagMap(String locationKeyRoot) {
		
		HashMap<String, Integer> sortingTagMap = new HashMap<>();
		
		String locationKey = locationKeyRoot + ":999";
		sortingTagMap.put(locationKey, 999);
		return sortingTagMap;
	}
	
}
