package com.vadenent;

import java.util.List;

public class FormFriends {
	
	private List<String> listOfFriends;

	public List<String> getListOfFriends() {
		return listOfFriends;
	}

	public void setListOfFriends(List<String> listOfFriends) {
		this.listOfFriends = listOfFriends;
	}

	
	
	@Override
	public String toString() {
		return "FormFriends [listOfFriends=" + listOfFriends + "]";
	}
	

}
