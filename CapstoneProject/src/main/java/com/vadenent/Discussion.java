package com.vadenent;

import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Discussion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String movieName;
	private String watchgroupName;
	private String locationKeyRoot;
	private Long movieId;
	private Long watchgroupId;
	private boolean publicDiscussion;

	@Column(columnDefinition="BLOB")
	private HashMap<String, Integer> sortingTagMap;
	
	
	public boolean isPublicDiscussion() {
		return publicDiscussion;
	}
	public void setPublicDiscussion(boolean publicDiscussion) {
		this.publicDiscussion = publicDiscussion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getWatchgroupName() {
		return watchgroupName;
	}
	public void setWatchgroupName(String watchgroupName) {
		this.watchgroupName = watchgroupName;
	}
	public String getLocationKeyRoot() {
		return locationKeyRoot;
	}
	public void setLocationKeyRoot(String locationKeyRoot) {
		this.locationKeyRoot = locationKeyRoot;
	}
	public Long getMovieId() {
		return movieId;
	}
	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	public Long getWatchgroupId() {
		return watchgroupId;
	}
	public void setWatchgroupId(Long watchgroupId) {
		this.watchgroupId = watchgroupId;
	}
	public HashMap<String, Integer> getSortingTagMap() {
		return sortingTagMap;
	}
	public void setSortingTagMap(HashMap<String, Integer> sortingTagMap) {
		this.sortingTagMap = sortingTagMap;
	}
	
	
	@Override
	public String toString() {
		return "Discussion [id=" + id + ", movieName=" + movieName + ", watchgroupName=" + watchgroupName
				+ ", locationKeyRoot=" + locationKeyRoot + ", movieId=" + movieId + ", watchgroupId=" + watchgroupId
				+ ", publicDiscussion=" + publicDiscussion + ", sortingTagMap=" + sortingTagMap + "]";
	}
	

}
