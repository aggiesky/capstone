package com.vadenent;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.SessionScope;

@SpringBootApplication
public class CapstoneProjectApplication {

	@Autowired
	UserDataRepository userDataRepository;
	
	@Autowired
	WatchgroupRepository watchgroupRepository;
	
	@Autowired
	DiscussionRepository discussionRepository;
	
	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	FriendRepository friendRepository;
	
	
	
	public static void main(String[] args) {
		SpringApplication.run(CapstoneProjectApplication.class, args);
	}

	
	
	// All views and controllers require this bean
	@Bean
	@SessionScope
	public LoginStatus createLoginStatus() {
		LoginStatus loginStatus = new LoginStatus();
		return loginStatus;
	}
	
	
	// place to hold movie DB search results
	@Bean(name = "movieSearchResults")
	@SessionScope
	public List<MovieDBMovie> createMSR() {
		List<MovieDBMovie> movie = new ArrayList<>();
		return movie;
	}
	
	
	
	// Miscellaneous session state stored in this object
	@Bean
	@SessionScope
	public SessionState createSessionState() {
		SessionState info = new SessionState();
		info.setHomeViewDiscussionId(1L);				// Initialized to a specific public discussion so the first app page has content.  
														// Must be a Public watchgroup discussion.
		info.setDiscussionViewDiscussionId(2L);			// Probably doesn't need to be initialized.
		info.setHomeViewFilterMovieId(0L);				// main page comment filter is set to show all Public comments
		info.setSelectedWatchgroupName("Create a New Watchgroup");	// default watchgroup action is to create a new one
		
		return info;
	}
	
	
	@Bean(name = "activeUsers")
	@ApplicationScope
	public TreeSet<UserData> createActiveUsers() {
		TreeSet<UserData> activeUsers = new TreeSet<>();
		return activeUsers;
	}
	
	// initialize the repositories for testing - avoids having to manually enter so much 
	// each test.
	@PostConstruct
	public void testInitUserDataRepository() {
		
		// Initialize the 
		List<String> registeredUsers = new ArrayList<>();

		List<String> friends1 = new ArrayList<>();
		List<String> friends2 = new ArrayList<>();
		
		friends1.add("Jenn");
		friends1.add("James");
		friends1.add("Andy");
		friends1.add("Liam");
		friends1.add("Chris");
		friends1.add("Lena");
		friends1.add("Jon");
		friends1.add("Glenne");
		friends1.add("Theresa");
		friends1.add("Charlie");
		
		friends2.add("Mike");
		friends2.add("Cindy");
		friends2.add("David");
		friends2.add("Sarah");
		friends2.add("Peter");
		friends2.add("Paul");
		friends2.add("Henry");
		friends2.add("Lucia");
		friends2.add("Tom");
		friends2.add("Laura");
		
		registeredUsers.addAll(friends1);
		registeredUsers.addAll(friends2);
				
		int count = 1;
		
		for ( String registeredUser : registeredUsers) {
			
			List<Friend> newListOfFriendObjects = new ArrayList<>();
			
			UserData tmpUserData = new UserData();
		    String theSalt = PasswordValidator.generateSalt();
		    String hashedPassword = PasswordValidator.hash("nnn", theSalt);

		    tmpUserData.setUserName(registeredUser);
		    tmpUserData.setSalt(theSalt);
		    tmpUserData.setPassword(hashedPassword);
		    
		    if (count <=10) {
		    	newListOfFriendObjects = friendListGenerator(true);
		    	tmpUserData.setFriends(newListOfFriendObjects);
		    } else {
		    	newListOfFriendObjects = friendListGenerator(false);
		    	tmpUserData.setFriends(newListOfFriendObjects);
		    }
		    count++;
		    
		    
			userDataRepository.save(tmpUserData);
			
		}
		
		return;
		
	}
	
	
	// helper method for the list of friends
	List<Friend> friendListGenerator(boolean isFirstList) {
		
		List<String> friends1 = new ArrayList<>();
		List<String> friends2 = new ArrayList<>();
		
		friends1.add("Jenn");
		friends1.add("James");
		friends1.add("Andy");
		friends1.add("Liam");
		friends1.add("Chris");
		friends1.add("Lena");
		friends1.add("Jon");
		friends1.add("Glenne");
		friends1.add("Theresa");
		friends1.add("Charlie");
		
		friends2.add("Mike");
		friends2.add("Cindy");
		friends2.add("David");
		friends2.add("Sarah");
		friends2.add("Peter");
		friends2.add("Paul");
		friends2.add("Henry");
		friends2.add("Lucia");
		friends2.add("Tom");
		friends2.add("Laura");
		
		List<Friend> listOfFriendObjects1 = new ArrayList<>();
		List<Friend> listOfFriendObjects2 = new ArrayList<>();
		
		Long tmpId = 1L;
		
		for (String friend : friends1) {
			Friend friendObject = new Friend();
			friendObject.setUsername(friend);
			friendObject.setUserId(tmpId++);
			friendObject.setOnLine(false);
//			friendRepository.save(friendObject);			this is no longer needed after adding cascade attribute to @OneToMany on UserData.List<Friend>
			listOfFriendObjects1.add(friendObject);	
		}
		for (String friend : friends2) {
			Friend friendObject = new Friend();
			friendObject.setUsername(friend);
			friendObject.setUserId(tmpId++);
			friendObject.setOnLine(false);
	//		friendRepository.save(friendObject);
			listOfFriendObjects2.add(friendObject);	
		}

		if (isFirstList)
			return listOfFriendObjects1;
		else
			return listOfFriendObjects2;
		
	}
	
	
	
	
	// Initialize Watchgroups
	@PostConstruct
	public void initWatchgroup() {
		
		// Create the "Public" watchgroup
		Watchgroup group1 = new Watchgroup();
		List<String> members1 = new ArrayList<>();
		
		group1.setMembers(members1);
		group1.setWatchgroupName("Public");
		group1.setCreator("");
		group1.setPublicWatchgroup(true);
	
		// save the new watchgroup
		watchgroupRepository.save(group1);

			
		// Create another watchgroup
		Watchgroup group2 = new Watchgroup();
		List<String> members2 = new ArrayList<>();
		members2.add("Mike");
		members2.add("Cindy");
		members2.add("David");
		members2.add("Sarah");
		
		group2.setMembers(members2);
		group2.setWatchgroupName("Western Fans");
		group2.setCreator("Mike");
		group2.setPublicWatchgroup(false);
		
		// save the new watchgroup
		watchgroupRepository.save(group2);

		
		// Create another watchgroup
		Watchgroup group3 = new Watchgroup();
		List<String> members3 = new ArrayList<>();
		members3.add("Jenn");
		members3.add("Jon");
		members3.add("Chris");
		
		group3.setMembers(members3);
		group3.setWatchgroupName("SciFi");
		group3.setCreator("Chris");
		group3.setPublicWatchgroup(false);
		
		// save the new watchgroup
		watchgroupRepository.save(group3);
	
		
		// Create another watchgroup
		Watchgroup group4 = new Watchgroup();
		List<String> members4 = new ArrayList<>();
		members4.add("Mike");
		members4.add("Cindy");
		members4.add("Tom");
		members4.add("Laura");
		
		group4.setMembers(members4);
		group4.setWatchgroupName("SpyThriller");
		group4.setCreator("Mike");
		group4.setPublicWatchgroup(false);
		
		// save the new watchgroup
		watchgroupRepository.save(group4);
			

	}
	
	
	
	// Initialize Discussions
	@PostConstruct
	public void testInitDiscussion() {

		//===========================================================
		// Create a discussion using the True Grit movie and the public watchgroup "Public"

		Discussion discussion = new Discussion();

		discussion.setMovieId(1L);					// 			
		discussion.setWatchgroupId(1L);				// hardcode id=1 for the Public watchgroup
		discussion.setMovieName("True Grit");
		discussion.setWatchgroupName("Public");
		discussion.setLocationKeyRoot("discussion000001");
		discussion.setPublicDiscussion(true);		
		
		String locationKeyRoot = discussion.getLocationKeyRoot();
		SortingHat sortingHat = new SortingHat();
		HashMap<String, Integer> sortingTagMap = sortingHat.createSortingTagMap(locationKeyRoot);
		discussion.setSortingTagMap(sortingTagMap);
		
		sortingTagMap.put(locationKeyRoot + ":998", 999);

		// save the new discussion
		discussionRepository.save(discussion);

		
		//===========================================================
		// Create a discussion using the True Grit movie and the private watchgroup "Western Fans"

		Discussion discussion2 = new Discussion();

		discussion2.setMovieId(1L);
		discussion2.setWatchgroupId(2L);
		discussion2.setMovieName("True Grit");
		discussion2.setWatchgroupName("Western Fans");
		discussion2.setLocationKeyRoot("discussion000002");
		discussion2.setPublicDiscussion(false);		

		SortingHat sortingHat2 = new SortingHat();
		HashMap<String, Integer> sortingTagMap2 = sortingHat2.createSortingTagMap(discussion2.getLocationKeyRoot());
		discussion2.setSortingTagMap(sortingTagMap2);

		// Initialize sortingTagMap entries to match the comments defined in the data.sql file
		locationKeyRoot = discussion2.getLocationKeyRoot();
		
		sortingTagMap2.put(locationKeyRoot + ":998", 999);
		sortingTagMap2.put(locationKeyRoot + ":997", 999);
		sortingTagMap2.put(locationKeyRoot + ":996", 999);
		sortingTagMap2.put(locationKeyRoot + ":995", 999);
		sortingTagMap2.put(locationKeyRoot + ":994", 999);

		
		// save the new discussion
		discussionRepository.save(discussion2);

		
		//===========================================================
		// Create a discussion using the Star Wars movie and the private watchgroup "SciFi"

		Discussion discussion3 = new Discussion();

		discussion3.setMovieId(2L);
		discussion3.setWatchgroupId(1L);
		discussion3.setMovieName("Star Wars - A New Hope");
		discussion3.setWatchgroupName("Public");
		discussion3.setLocationKeyRoot("discussion000003");
		discussion3.setPublicDiscussion(true);		

		SortingHat sortingHat3 = new SortingHat();
		HashMap<String, Integer> sortingTagMap3 = sortingHat3.createSortingTagMap(discussion3.getLocationKeyRoot());
		discussion3.setSortingTagMap(sortingTagMap3);

		locationKeyRoot = discussion3.getLocationKeyRoot();
		sortingTagMap3.put(locationKeyRoot + ":998", 999);
		sortingTagMap3.put(locationKeyRoot + ":997", 999);

		// save the new discussion
		discussionRepository.save(discussion3);	
		
		
	}
		
	
}
