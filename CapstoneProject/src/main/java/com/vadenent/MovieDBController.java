package com.vadenent;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/")
public class MovieDBController {

	private final String POSTER_BASE_URL = "https://image.tmdb.org/t/p/w185";
	private final String BACKDROP_BASE_URL = "https://image.tmdb.org/t/p/original";
	private final String MOVIEDB_BASE_URL = "https://api.themoviedb.org/3";
	private final String API_KEY = "api_key=a2a3465ca7e2c301e715321ca4e37390";
	
	
	@Autowired
	List<MovieDBMovie> movieSearchResults;
	
	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	MovieRepository movieRepository;
	
	
	
	//---------------------------------------------------------------------------------------------
	// Search for movie via Title - user provides a search string
	//---------------------------------------------------------------------------------------------
	@GetMapping("/search/movie")
	public String getSearchMovie(Model model) {
		
		FormMovieSearch formMovieSearch = new FormMovieSearch();
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("formMovieSearch", formMovieSearch);
		
		
		return "movieSearch";
	}
	
	

	@PostMapping("/search/movie")
	public String postSearchMovie(Model model, FormMovieSearch formMovieSearch) throws UnsupportedEncodingException {
		
		WebClient webClient = WebClient.create(MOVIEDB_BASE_URL 
				+ "/search/movie?api_key=a2a3465ca7e2c301e715321ca4e37390&language=en-US&include_adult=false&query=" 
				+ URLEncoder.encode(formMovieSearch.getSearchString(),"UTF-8"));
		
		Mono<SearchResults> searchResults = webClient.get()
						.retrieve()
						.bodyToMono(SearchResults.class);
		
		movieSearchResults = searchResults.block().getResults();
		
		return "redirect:/search/results";
		
	}

	
	//-----------------------------------------------------------------------------------------
	// Select a movie from the search results
	//-----------------------------------------------------------------------------------------
	@GetMapping("/search/results")
	public String getSearchResults(Model model) {
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("listOfMovies", movieSearchResults);
		
		MovieDBMovie selectedMovie = new MovieDBMovie();
		model.addAttribute("formSelectedMovie", selectedMovie);
		
		return "movieSearchResults";
	}
	
	
	
	@PostMapping("/search/results")
	public String postSearchResults(Model model, MovieDBMovie formSelectedMovie, RedirectAttributes redirect) {
		
		model.addAttribute("loginStatus", loginStatus);
				
		WebClient webClient = WebClient.create(
				MOVIEDB_BASE_URL + "/movie/" 
			  + formSelectedMovie.getId() 
			  + "?" 
			  + API_KEY);
		
		
		Mono<MovieDetails> searchResults = webClient.get()
						.retrieve()
						.bodyToMono(MovieDetails.class);
		
		MovieDetails responseMovieDetails = searchResults.block();
			
		String backdropImagePath = BACKDROP_BASE_URL + responseMovieDetails.getBackdrop_path();
		String posterImagePath =   POSTER_BASE_URL + responseMovieDetails.getPoster_path();

		Movie newMovie = new Movie();
		
		newMovie.setMovieTitle(responseMovieDetails.getTitle());
		newMovie.setMovieDescription(responseMovieDetails.getOverview());
		newMovie.setReleaseYear(responseMovieDetails.getRelease_date().substring(0,4));  // year is the 1st 4 characters
		newMovie.setBackdropImage(backdropImagePath);
		newMovie.setPosterImage(posterImagePath);
		newMovie.setGenre(responseMovieDetails.getGenres().get(0).getName());
		
		movieRepository.save(newMovie);
		
		
		redirect.addFlashAttribute("message",  "You successfully added the movie: " + newMovie.getMovieTitle());
		return "redirect:/";
	}
	

}
