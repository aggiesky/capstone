package com.vadenent;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;


@Controller
@RequestMapping("/")
public class ControllerDiscussion {
	
	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	MovieRepository movieRepository;
	
	@Autowired
	DiscussionRepository discussionRepository;
	
	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	BuildNewComment bnc;
	
	@Autowired
	SessionState sessionState;
	
	@Autowired
	SortingHat sortingHat;
	
	@Autowired
	UserDataRepository userDataRepository;
	
	
	
	@GetMapping("/discussion")
	public String showData(Model model) {
		
		// test: if the user is not logged in, don't show this page; return immediately
		if (!loginStatus.isLoggedIn()) {
			return "redirect:/";
		}
		
		// Get the discussionId which sets the context for what should be shown in the view based on the movieId and watchgroupId
		Long discussionId = sessionState.getDiscussionViewDiscussionId();
		if (discussionId == 0)	return "redirect:/";		// no-op if no discussion selection is made
		
		Optional<Discussion> optionalDiscussion = discussionRepository.findById(discussionId);
		Discussion selectedDiscussion = new Discussion();
		
		if (optionalDiscussion.isPresent()) 	selectedDiscussion = optionalDiscussion.get();
	
		Long movieId = selectedDiscussion.getMovieId();
		
		
		if (loginStatus.isLoggedIn()) {
			
			UserData userData = new UserData();
			Optional<UserData> optionalUserData = userDataRepository.findById(loginStatus.getUserId());
			if (optionalUserData.isPresent())  userData = optionalUserData.get();
			
			
			List<Friend> friends = userData.getFriends();
			for (Friend friend : friends) {				
				Optional<UserData> optionalTmpUserData = userDataRepository.findById(friend.getUserId());
				if (optionalTmpUserData.isPresent()) {
					UserData tmpUserData = optionalTmpUserData.get();
					friend.setOnLine(tmpUserData.isOnLine());
				}				
			}
						
			model.addAttribute("friends", friends);
			
		}		

		
		Optional<Movie> optionalMovie = movieRepository.findById(selectedDiscussion.getMovieId());
		
		if (optionalMovie.isPresent()) {
			model.addAttribute("discussion", selectedDiscussion);
			model.addAttribute("loginStatus", loginStatus);
			model.addAttribute("movie", optionalMovie.get());
			model.addAttribute("comments", 
				commentRepository.findByMovieIdAndDiscussionIdOrderByLocationKeyAsc(movieId, discussionId));
			
			return "discussion";
			
		} else {

			System.out.println("What? No movie?");
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	
	
	@PostMapping("/discussion")
	public String postDiscussion(Model model, @RequestParam Long discussionId) {

		if (loginStatus.isLoggedIn()) {
			sessionState.setDiscussionViewDiscussionId(discussionId);
			return "redirect:/discussion";
		} else {
			return "redirect:/";		// no-op if not logged in
		}
	}
	
	
	
	
	// User has requested to make a new Comment
	@PostMapping("/newComment")
	public String newCommentGet(Model model, 
								@RequestParam("parentKey") String mario,
								@RequestParam("viewName")  String viewName) {
		
		FormNewComment formNewComment = new FormNewComment();
		
		// Which page did request come from?  Read in discussionId based on the requesting page.
		// Then retrieve the discussion object, and get the movieId and watchgroupId.
		
		Long discussionId = 0L;
		
		if (viewName.equals("main")) {
			discussionId = sessionState.getHomeViewDiscussionId();
		} else {
			discussionId = sessionState.getDiscussionViewDiscussionId();
		}
		
		Discussion discussion = new Discussion();
		
		Optional<Discussion> optionalDiscussion = discussionRepository.findById(discussionId);
		
		if (optionalDiscussion.isPresent()) {
			discussion = optionalDiscussion.get();
		}
		
		Long movieId = discussion.getMovieId();
		Long watchgroupId = discussion.getWatchgroupId();
		
		
		// Populate the Model with the objects needed for the view.
		Optional<Movie> optionalMovie = movieRepository.findById(movieId);
		
		if (optionalMovie.isPresent()) {
			model.addAttribute("loginStatus", loginStatus);
			model.addAttribute("movie", optionalMovie.get());
			model.addAttribute("comments", 
					commentRepository.findByMovieIdAndWatchgroupIdOrderByLocationKeyAsc(movieId, watchgroupId));
			model.addAttribute("parentKey", mario);
			model.addAttribute("formNewComment", formNewComment);
			model.addAttribute("discussion", discussion);
			
			// return to the same page that the request came from.
			if (viewName.equals("main")) 
				return "main";
			else
				return "discussion";
				
		} else {

			System.out.println("What? No movie?");
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

	}
	
	
	
	@PostMapping("/newComment/{parentKey}")
	public String newCommentPost(Model model, 
								 @PathVariable("parentKey") String luigi,
								 @RequestParam("viewName") String viewName,
								 FormNewComment formNewComment) {
		
		// Which page did request come from?  Read in discussionId based on the requesting page.
		// Then retrieve the discussion object, and get the movieId and watchgroupId.
		
		Long discussionId = 0L;		// overwritten next
		
		if (viewName.equals("main")) {
			discussionId = sessionState.getHomeViewDiscussionId();
		} else {
			discussionId = sessionState.getDiscussionViewDiscussionId();
		}

		
		// Get the current discussion
		Discussion discussion = sortingHat.getDiscussionObject(viewName);
		Long movieId = discussion.getMovieId();
		Long watchgroupId = discussion.getWatchgroupId();
		
		Comment parentComment = new Comment();
		
		// The "parentKey" request parameter holds the locationKey of the parent Comment.
		// The newComment is being initialized with the data from the parent Comment - several
		//   fields will be updated with the appropriate info from the new comment form.
		parentComment = commentRepository.findFirstByLocationKeyAndDiscussionId(luigi, discussionId);
		String locationKey = parentComment.getLocationKey();
		
		// gather up all the data for the newComment which will be added to the repository
		// parentComment, formDataNewComment, and loginStatus supply the source info
		
		Comment newComment = bnc.buildNewComment(locationKey, 
												 formNewComment.getCommentTitle(),
												 formNewComment.getCommentText(),
												 discussion);
		
		// newComment is added to the repository, the Id has not been initialized
		// so this is an "insert", not an "update".
		commentRepository.save(newComment);			// Save the new Comment to the database
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("comments", commentRepository.findByMovieIdAndWatchgroupIdOrderByLocationKeyAsc(movieId, watchgroupId));
		
		if (viewName.equals("main"))
			return "redirect:/";
		else
			return "redirect:/discussion";
		
	}
	
	
	
}
