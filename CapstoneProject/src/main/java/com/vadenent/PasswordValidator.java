package com.vadenent;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PasswordValidator {
	
	private static final int ITERATIONS = 10000;
	private static final int KEY_LENGTH = 256;

	// check the entered inputPassword against the saved databasePassword
	// inputPassword parameter must not be hashed
	// databasePassword parameter must already be hashed
	// databaseSalt is the value saved along with the saved password for existing user (databasePassword)
	public static boolean isPasswordValid(String inputPassword, String databasePassword, String databaseSalt) {

		// add hashing on the input password
		// get the saved password which is already hashed
		// compare the two - true iff they match

		String hashedInputPassword = hash(inputPassword, databaseSalt);

		if (databasePassword.equals(hashedInputPassword))
			return true;
		else
			return false;

	}

	
	public static String hash(String password, String databaseSalt) {
		return Base64.getEncoder().encodeToString(owaspHash(password.toCharArray(), databaseSalt.getBytes(), ITERATIONS, KEY_LENGTH));
	}

	
	
	public static String generateSalt() {
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[4];
		random.nextBytes(salt);
		return Base64.getEncoder().encodeToString(salt);
	}


	
	// https://www.owasp.org/index.php/Hashing_Java
	private static byte[] owaspHash(final char[] password, final byte[] salt, final int iterations,
			final int keyLength) {

		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
			PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
			SecretKey key = skf.generateSecret(spec);
			byte[] res = key.getEncoded();
			return res;

		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

}
