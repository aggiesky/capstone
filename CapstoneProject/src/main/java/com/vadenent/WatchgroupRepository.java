package com.vadenent;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface WatchgroupRepository extends CrudRepository<Watchgroup, Long> {
	
	Watchgroup findByWatchgroupName(String watchgroupName);
	List<Watchgroup> findByMembers(String member);

	
}
