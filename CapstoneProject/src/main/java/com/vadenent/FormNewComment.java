package com.vadenent;

public class FormNewComment {
	
	private String commentTitle;
	private String commentText;
	
	
	public String getCommentTitle() {
		return commentTitle;
	}
	public void setCommentTitle(String commentTitle) {
		this.commentTitle = commentTitle;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	
	
	@Override
	public String toString() {
		return "FormDataNewComment [title=" + commentTitle + ", commentText=" + commentText + "]";
	}

}
