package com.vadenent;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;




@Controller
@RequestMapping("/")
public class ControllerLogin {

	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	UserDataRepository userDataRepository;

	
	
	
	@GetMapping("/login")
	public String loginView(Model model) {
		
		FormLogin formLogin = new FormLogin();
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("formLogin", formLogin);
		return "login";
	}
	
	
	
	
	@PostMapping("/login")
	public String loginUpdate(Model model, FormLogin formLogin, RedirectAttributes redirect) {
		
		model.addAttribute("loginStatus", loginStatus);
		//model.addAttribute("formDataLogin", formDataLogin);
		
		UserData newUser = new UserData();
		
		newUser.setUserName(formLogin.getUserName());
		newUser.setPassword(formLogin.getPassword());
		
		UserData userData = userDataRepository.findByUserName(newUser.getUserName());
		
		newUser.setId(userData.getId());

		// Only validate password if:
		// - input userName is not null
		// - the userName is found as a valid user in the user table (if it is not, then userData is null)
		if ( (newUser.getUserName() != null) && (userData != null) ) { 	
			
			if (PasswordValidator.isPasswordValid(newUser.getPassword(), userData.getPassword(), userData.getSalt())) {
	
				//set current User as logged in; place in the model (session scope)
				loginStatus.loginAs(newUser.getUserName(), newUser.getId());
				
				// update the "onLine" status in userData for this user
				userData.setOnLine(true);
				userDataRepository.save(userData);
				
				//set flash message - login successful
				redirect.addFlashAttribute("message", "You logged in successfully as '" + formLogin.getUserName() + "'");
				return "redirect:/";

			} else {	// report invalid password
				model.addAttribute("message", "Login failed.  Try Again.");
				return "login";
			}
				
		} else {	// report the user is not registered - please register first
			model.addAttribute("message", "The 'Username' is not registered.  Please select 'New User Registration'");
			return "login";
		}
		

	}
	
	
		
	
	// === logout function ==========================================
	@GetMapping("/logout")
	public String logoutView(Model model, RedirectAttributes redirect) {
		
		// test: if the user is not logged in, then it doesn't make sense to logout; return immediately.
		if (!loginStatus.isLoggedIn()) {
			return "redirect:/";
		}
		
		
		Optional<UserData> optionalUserData = userDataRepository.findById(loginStatus.getUserId());
		
		UserData userData = new UserData();
		
		// onLine status will no show offline
		if (optionalUserData.isPresent()) {
			userData = optionalUserData.get();
			userData.setOnLine(false);
			userDataRepository.save(userData);
		}
		
				
		model.addAttribute("loginStatus", loginStatus);
		
		loginStatus.logout();

		redirect.addFlashAttribute("message", "You have logged out.  Good-bye.");

		return "redirect:/";
	}


	
}
