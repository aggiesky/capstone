package com.vadenent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Movie implements Comparable<Movie> {

	@javax.persistence.Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long Id;
	
	private String movieTitle;
	private String backdropImage;
	private String posterImage;
	private String releaseYear;
	private String movieAltText;
	

	@Column(length=4192)
	private String movieDescription;
	private String genre;
	
	
	// getters / setters
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getMovieTitle() {
		return movieTitle;
	}
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	public String getBackdropImage() {
		return backdropImage;
	}
	public void setBackdropImage(String movieImage) {
		this.backdropImage = movieImage;
	}
	public String getPosterImage() {
		return posterImage;
	}
	public void setPosterImage(String posterImage) {
		this.posterImage = posterImage;
	}
	public String getReleaseYear() {
		return releaseYear;
	}
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	public String getMovieAltText() {
		return movieAltText;
	}
	public void setMovieAltText(String movieAltText) {
		this.movieAltText = movieAltText;
	}
	public String getMovieDescription() {
		return movieDescription;
	}
	public void setMovieDescription(String movieDescription) {
		this.movieDescription = movieDescription;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	// Use the Id field for sorting
	@Override
	public int compareTo(Movie movie) {	
		if (this.Id == movie.getId())      return 0;
		else if (this.Id > movie.getId())  return 1;
		else                               return -1;
	}
	
	
	
	@Override
	public String toString() {
		return "Movie [Id=" + Id + ", movieTitle=" + movieTitle + ", backdropImage=" + backdropImage + ", posterImage="
				+ posterImage + ", releaseYear=" + releaseYear + ", movieAltText=" + movieAltText
				+ ", movieDescription=" + movieDescription + ", genre=" + genre + "]";
	}
	
	
	
	
}
