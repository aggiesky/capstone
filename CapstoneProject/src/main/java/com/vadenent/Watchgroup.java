package com.vadenent;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Watchgroup {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ElementCollection
	private List<String> members;
	private boolean publicWatchgroup;
	private String creator;
	
	@NotNull
	@Size(min=5, max=40)
	private String watchgroupName;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<String> getMembers() {
		return members;
	}
	public void setMembers(List<String> members) {
		this.members = members;
	}
	public boolean isPublicWatchgroup() {
		return publicWatchgroup;
	}
	public void setPublicWatchgroup(boolean publicWatchgroup) {
		this.publicWatchgroup = publicWatchgroup;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getWatchgroupName() {
		return watchgroupName;
	}
	public void setWatchgroupName(String watchgroupName) {
		this.watchgroupName = watchgroupName;
	}
	
	
	
	@Override
	public String toString() {
		return "Watchgroup [id=" + id + ", members=" + members + ", publicWatchgroup=" + publicWatchgroup + ", creator="
				+ creator + ", watchgroupName=" + watchgroupName + "]";
	}
	

}
