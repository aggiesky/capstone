package com.vadenent;

import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;

public class LoginStatus {
	
	@Autowired
	TreeSet<UserData> activeUsers;
	
	private boolean loggedIn;
	private String userName;
	private Long userId;
	
    public boolean isLoggedIn() {
		return loggedIn;
	}

	public String getUserName() {
		return userName;
	}

	public Long getUserId() {
		return userId;
	}

	
	public void logout() {
		
		//remove user from the application state
		UserData userData = new UserData();
		userData.setId(this.userId);
		activeUsers.remove(userData);
		
		// remove user from the session state
		this.loggedIn = false;
		this.userName = null;
		this.userId = 0L;

	}
	
	public void loginAs(String userName, Long userId) {
		
		// update session status with the id/username of the user that is logging in now
		this.loggedIn = true;
		this.userName = userName;
		this.userId = userId;
		
		// update the application state with the username/id - presence in the Set indicates user is logged in.
		UserData userData = new UserData();
		userData.setUserName(userName);
		userData.setId(userId);
		activeUsers.add(userData);
	}

	
	@Override
	public String toString() {
		return "LoginStatus [loggedIn=" + loggedIn + ", userName=" + userName + ", userId=" + userId + "]";
	}

	
}
