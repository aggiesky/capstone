package com.vadenent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Comment {
	
	@javax.persistence.Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long Id;
	
	private Long movieId;
	private String movieTitle;
	private Long watchgroupId;
	private String watchgroupLabel;
	private Long ownerId;
	private String ownerName;
	private String commentTitle;
	
	@Column(length=2048)
	private String commentText;
	
	private String locationKey;
	private String commentDateTime;
	private Long discussionId;
	private int indent;
	private int likes;
	private int dislikes;
	private int headslaps;
	

	
	// Getters and Setters

	
	public String getCommentDateTime() {
		return commentDateTime;
	}
	public void setCommentDateTime(String commentDateTime) {
		this.commentDateTime = commentDateTime;
	}


	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Long getMovieId() {
		return movieId;
	}
	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	public String getMovieTitle() {
		return movieTitle;
	}
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	public Long getWatchgroupId() {
		return watchgroupId;
	}
	public void setWatchgroupId(Long watchgroupId) {
		this.watchgroupId = watchgroupId;
	}
	public String getWatchgroupLabel() {
		return watchgroupLabel;
	}
	public void setWatchgroupLabel(String watchgroupLabel) {
		this.watchgroupLabel = watchgroupLabel;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getCommentTitle() {
		return commentTitle;
	}
	public void setCommentTitle(String commentTitle) {
		this.commentTitle = commentTitle;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public String getLocationKey() {
		return locationKey;
	}
	public void setLocationKey(String locationKey) {
		this.locationKey = locationKey;
	}
	public Long getDiscussionId() {
		return discussionId;
	}
	public void setDiscussionId(Long discussionId) {
		this.discussionId = discussionId;
	}
	public int getIndent() {
		return indent;
	}
	public void setIndent(int indent) {
		this.indent = indent;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public int getDislikes() {
		return dislikes;
	}
	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}
	public int getHeadslaps() {
		return headslaps;
	}
	public void setHeadslaps(int headslaps) {
		this.headslaps = headslaps;
	}
	
	
	@Override
	public String toString() {
		return "Comment [Id=" + Id + ", movieId=" + movieId + ", movieTitle=" + movieTitle + ", watchgroupId="
				+ watchgroupId + ", watchgroupLabel=" + watchgroupLabel + ", ownerId=" + ownerId + ", ownerName="
				+ ownerName + ", commentTitle=" + commentTitle + ", commentText=" + commentText + ", locationKey="
				+ locationKey + ", commentDateTime="
				+ commentDateTime + ", discussionId=" + discussionId
				+ ", indent=" + indent + ", likes=" + likes + ", dislikes=" + dislikes + ", headslaps=" + headslaps
				+ "]";
	}
	
	
}
