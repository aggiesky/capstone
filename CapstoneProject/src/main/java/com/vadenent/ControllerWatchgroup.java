package com.vadenent;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/")
public class ControllerWatchgroup {
	
	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	UserDataRepository userDataRepository;
	
	@Autowired
	WatchgroupRepository watchgroupRepository;
	
	@Autowired
	SessionState sessionState;
	


	
	@GetMapping("/watchgroup")
	public String getWatchgroup(Model model) {
		
		//---------------------------------------------------------------------------------
		// Must be logged in for this view
		
		if (!loginStatus.isLoggedIn())  {
			return "redirect:/";
		}
	
		
		//---------------------------------------------------------------------------------
		// Get the list of all watchgroups
		List<Watchgroup> watchgroups = (List<Watchgroup>) watchgroupRepository.findByMembers(loginStatus.getUserName());
		List<String> listOfWatchgroups = new ArrayList<>();
		
		listOfWatchgroups.add("Create a New Watchgroup");
		for (Watchgroup watchgroup : watchgroups ) {
			listOfWatchgroups.add(watchgroup.getWatchgroupName());
		}
		
		
		//---------------------------------------------------------------------------------
		// Get the user's list of Friends	
		UserData tmpUserData = userDataRepository.findByUserName(loginStatus.getUserName());
		List<Friend> friends = tmpUserData.getFriends();
		List<String> listOfFriends = new ArrayList<>();
		for (Friend friend : friends) {
			listOfFriends.add(friend.getUsername());
		}

		
		//---------------------------------------------------------------------------------
		// Load the formWatchgroup.  If it is a "New Watchgroup", then don't try to retrieve anything
		// from the respository.
		// - coming from Navbar, leave formWatchgroup null; no watchgroup will be selected when page shows
		// - otherwise, populate formWatchgroup;
		FormWatchgroup formWatchgroup = new FormWatchgroup();
		String currentWatchgroupName = sessionState.getSelectedWatchgroupName();
		
		if (!currentWatchgroupName.equals("Create a New Watchgroup")) {
			Watchgroup currentWatchgroup = watchgroupRepository.findByWatchgroupName(currentWatchgroupName);
			formWatchgroup.setMembers(currentWatchgroup.getMembers());
			formWatchgroup.setWatchgroupName(currentWatchgroupName);
		}
		
		
		
		//---------------------------------------------------------------------------------
		// add to model
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("friends", listOfFriends);
		model.addAttribute("listOfWatchgroups", listOfWatchgroups);
		model.addAttribute("formWatchgroup", formWatchgroup);
							
		return "watchgroup";
		
	}
	
	

	
	@PostMapping("/watchgroup/select")
	public String postWatchgroupSelect(Model model, 
			@RequestParam(defaultValue = "inactive") String selectedWatchgroup,
			RedirectAttributes redirect) {
		
		if (! "inactive".equals(selectedWatchgroup)) {
			
			// remove trailing comma if it exists - seems to be appended by the binder
			if (selectedWatchgroup.endsWith(",")) {
				selectedWatchgroup = selectedWatchgroup.substring(0,selectedWatchgroup.lastIndexOf(","));
			}
			
			sessionState.setSelectedWatchgroupName(selectedWatchgroup);
			
			return "redirect:/watchgroup";
			
		} else {
			
			return "watchgroup";
			
		}
		
	}
		

	
	
	@PostMapping("/watchgroup")
	public String postWatchgroup(Model model, 
			FormWatchgroup formWatchgroup,
			RedirectAttributes redirect) {
								
			// read/modify/write to update the Members from the form
			Watchgroup theWatchgroup = 
					watchgroupRepository.findByWatchgroupName(formWatchgroup.getWatchgroupName());

			// not null indicates this is an existing watchgroup; update members and save
			if (theWatchgroup != null) {
				theWatchgroup.setMembers(formWatchgroup.getMembers());
				watchgroupRepository.save(theWatchgroup);
				redirect.addFlashAttribute("message", 
						"You successfully updated the watchgroup \"" + formWatchgroup.getWatchgroupName() + "\"");
				
			// null indicates this is a new watchgroup; populate all fields including 
			//	  new watchgroup name and member list. 
			} else {
				Watchgroup watchgroup = new Watchgroup();
				watchgroup.setCreator(loginStatus.getUserName());
				watchgroup.setPublicWatchgroup(false);
				watchgroup.setWatchgroupName(formWatchgroup.getWatchgroupName());
				watchgroup.setMembers(formWatchgroup.getMembers());
				watchgroupRepository.save(watchgroup);
				redirect.addFlashAttribute("message", 
						"You successfully added the new watchgroup \"" + formWatchgroup.getWatchgroupName() + "\"");
			}
			
			return "redirect:/";
			
	}

		
}
