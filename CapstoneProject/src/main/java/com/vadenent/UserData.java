package com.vadenent;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;

@Entity
public class UserData implements Comparable<UserData> {

	@javax.persistence.Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long   id;
	
	private String userName;
	private String password;
	private String salt;
	private boolean onLine;
	

	@OneToMany(cascade = CascadeType.ALL)
	private List<Friend> friends;

	// getters / setters
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
		
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	public boolean isOnLine() {
		return onLine;
	}
	public void setOnLine(boolean onLine) {
		this.onLine = onLine;
	}

	public List<Friend> getFriends() {
		return friends;
	}
	public void setFriends(List<Friend> friends) {
		this.friends = friends;
	}
	
	// Use the Id field for sorting
	@Override
	public int compareTo(UserData userData) {	
		if (this.id == userData.getId())      return 0;
		else if (this.id > userData.getId())  return 1;
		else                          		  return -1;
	}
	
	
	
	@Override
	public String toString() {
		return "UserData [id=" + id + ", userName=" + userName + ", password=" + password + ", salt=" + salt
				+ ", onLine=" + onLine + ", friends=" + friends + "]";
	}
	
	
}
