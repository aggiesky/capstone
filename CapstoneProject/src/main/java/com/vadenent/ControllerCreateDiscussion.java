package com.vadenent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;




@Controller
@RequestMapping("/")
public class ControllerCreateDiscussion {
	
	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	MovieRepository movieRepository;
	
	@Autowired
	DiscussionRepository discussionRepository;
	
	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	SortingHat sortingHat;
	
	@Autowired
	BuildNewComment bnc;
	
	@Autowired
	WatchgroupRepository watchgroupRepository;
	

	final Long PUBLIC_WATCHGROUP = 1L;
	
	
	
	@GetMapping("/createDiscussion")
	public String getCreateDiscussion(Model model) {
	
		// Always need the loginStatus for the NavBar and possibly 
		// other features in the view template
		model.addAttribute("loginStatus", loginStatus);

		// Get the list of movies
		List<Movie> movies = (List<Movie>) movieRepository.findAll();
		
		// Get the list of watchgroups
		List<Watchgroup> watchgroups = (List<Watchgroup>) watchgroupRepository.findByMembers(loginStatus.getUserName());
		List<Watchgroup> groups = new ArrayList<>();
		
		Optional<Watchgroup> optionalWatchgroup = watchgroupRepository.findById(1L);  // the PUBLIC watchgroup
		
		if (optionalWatchgroup.isPresent())		groups.add(optionalWatchgroup.get());
		
		groups.addAll(watchgroups);
				
		// build form object
		FormCreateDiscussion formCreateDiscussion = new FormCreateDiscussion();
				
		model.addAttribute("formCreateDiscussion", formCreateDiscussion);
		model.addAttribute("movies", movies);
		model.addAttribute("groups", groups);
		
		return "createDiscussion";
	}
	

	
	
	
	@PostMapping("createDiscussion")
	public String postCreateDiscussion(Model model, 
									  FormCreateDiscussion formCreateDiscussion,
									  RedirectAttributes redirect) {
		
		
		Long movieId = formCreateDiscussion.getMovieId();
		Long watchgroupId = formCreateDiscussion.getWatchgroupId();
		Discussion existingDiscussion = discussionRepository.findByMovieIdAndWatchgroupId(movieId, watchgroupId);
		
		// if discussion exists already, return and show an error/info message
		if (existingDiscussion != null) {
			redirect.addFlashAttribute("message", "That discussion already exists.  Please choose a different combination.\n");
			return "redirect:/createDiscussion";
		}
		
		// populate the Discussion object from the form
		Discussion discussion = new Discussion();
		
		// Initialize the properties of the new Discussion
		discussion.setMovieId(formCreateDiscussion.getMovieId());
		discussion.setWatchgroupId(formCreateDiscussion.getWatchgroupId());
		
		if (discussion.getWatchgroupId() == 1L)		discussion.setPublicDiscussion(true);		// WatchgroupID = 1L is the PUBLIC watchgroup
		else										discussion.setPublicDiscussion(false);
		 
		
		Optional<Movie> optionalMovie = movieRepository.findById(discussion.getMovieId());
		Optional<Watchgroup> optionalWatchgroup = watchgroupRepository.findById(discussion.getWatchgroupId());
		
		if (optionalMovie.isPresent()) 			discussion.setMovieName(optionalMovie.get().getMovieTitle());
		if (optionalWatchgroup.isPresent())		discussion.setWatchgroupName(optionalWatchgroup.get().getWatchgroupName());
		
		
						
		// save the new discussion; after the 1st save, get the new id and
		// use it to calculate the "locationKeyRoot"; then save the updated discussion that contains the locationKeyRoot
		Discussion savedDiscussion = discussionRepository.save(discussion);
		
		String locationKeyRoot = String.format("discussion%06d", savedDiscussion.getId());
		savedDiscussion.setLocationKeyRoot(locationKeyRoot);

		SortingHat sortingHat = new SortingHat();
		HashMap<String, Integer> sortingTagMap = sortingHat.createSortingTagMap(locationKeyRoot);
		savedDiscussion.setSortingTagMap(sortingTagMap);
		
		discussionRepository.save(savedDiscussion);
		
		
		// Create new comment - the initial comment for the discussion just created.
		// BuildNewComment bnc = new BuildNewComment();
		Comment newComment = bnc.buildNewComment(locationKeyRoot+":999", 
												 formCreateDiscussion.getCommentTitle(), 
												 formCreateDiscussion.getCommentText(), 
												 savedDiscussion);
		
		commentRepository.save(newComment);
				
		redirect.addFlashAttribute("message", "You successfully created a new Discussion: "
											   + savedDiscussion.getWatchgroupName() + " : " + savedDiscussion.getMovieName());
		
		return "redirect:/";
		
	}

	
	
}
