package com.vadenent;

import org.springframework.data.repository.CrudRepository;


public interface UserDataRepository extends CrudRepository<UserData, Long> {
	
	UserData findByUserName(String UserName);

	
}
