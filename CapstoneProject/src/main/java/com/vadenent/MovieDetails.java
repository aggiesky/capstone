package com.vadenent;

import java.util.List;


public class MovieDetails {

	private boolean adult;
	private String backdrop_path;
	private int budget;
	private List<Genres> genres;
	private String homepage;
	private int id;
	private String imdb_id;
	private String original_language;
	private String original_title;
	private String overview;
	private double popularity;
	private String poster_path;
	private List<ProductionCompany> production_companies;
	private List<ProductionCountry> production_countries;
	private String release_date;
	private int revenue;
	private int runtime;
	private List<SpokenLanguage> spoken_languages;
	private String status;
	private String tagline;
	private String title;
	private boolean video;
	private double vote_average;
	private MovieCollection belongs_to_collection;
	private int vote_count;
	

	
	public boolean isAdult() {
		return adult;
	}
	public void setAdult(boolean adult) {
		this.adult = adult;
	}
	public String getBackdrop_path() {
		return backdrop_path;
	}
	public void setBackdrop_path(String backdrop_path) {
		this.backdrop_path = backdrop_path;
	}
	public MovieCollection getBelongs_to_collection() {
		return belongs_to_collection;
	}
	public void setBelongs_to_collection(MovieCollection belongs_to_collection) {
		this.belongs_to_collection = belongs_to_collection;
	}		
	public int getBudget() {
		return budget;
	}
	public void setBudget(int budget) {
		this.budget = budget;
	}

	public List<Genres> getGenres() {
		return genres;
	}
	public void setGenres(List<Genres> genres) {
		this.genres = genres;
	}  	

	public String getHomepage() {
		return homepage;
	}
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImdb_id() {
		return imdb_id;
	}
	public void setImdb_id(String imdb_id) {
		this.imdb_id = imdb_id;
	}
	public String getOriginal_language() {
		return original_language;
	}
	public void setOriginal_language(String original_language) {
		this.original_language = original_language;
	}
	public String getOriginal_title() {
		return original_title;
	}
	public void setOriginal_title(String original_title) {
		this.original_title = original_title;
	}
	public String getOverview() {
		return overview;
	}
	public void setOverview(String overview) {
		this.overview = overview;
	}
	public double getPopularity() {
		return popularity;
	}
	public void setPopularity(double popularity) {
		this.popularity = popularity;
	}
	public String getPoster_path() {
		return poster_path;
	}
	public void setPoster_path(String poster_path) {
		this.poster_path = poster_path;
	}
	public List<ProductionCompany> getProduction_companies() {
		return production_companies;
	}
	public void setProduction_companies(List<ProductionCompany> production_companies) {
		this.production_companies = production_companies;
	}
	public List<ProductionCountry> getProduction_countries() {
		return production_countries;
	}
	public void setProduction_countries(List<ProductionCountry> production_countries) {
		this.production_countries = production_countries;
	}		
	public String getRelease_date() {
		return release_date;
	}
	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}
	public int getRevenue() {
		return revenue;
	}
	public void setRevenue(int revenue) {
		this.revenue = revenue;
	}
	public int getRuntime() {
		return runtime;
	}
	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}
	public List<SpokenLanguage> getSpoken_languages() {
		return spoken_languages;
	}
	public void setSpoken_languages(List<SpokenLanguage> spoken_languages) {
		this.spoken_languages = spoken_languages;
	}		
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTagline() {
		return tagline;
	}
	public void setTagline(String tagline) {
		this.tagline = tagline;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isVideo() {
		return video;
	}
	public void setVideo(boolean video) {
		this.video = video;
	}
	public double getVote_average() {
		return vote_average;
	}
	public void setVote_average(double vote_average) {
		this.vote_average = vote_average;
	}
	public int getVote_count() {
		return vote_count;
	}
	public void setVote_count(int vote_count) {
		this.vote_count = vote_count;
	}
	
	
	@Override
	public String toString() {
		return "MovieDetails [adult=" + adult + ", backdrop_path=" + backdrop_path + ", budget=" + budget + ", genres="
				+ genres + ", homepage=" + homepage + ", id=" + id + ", imdb_id=" + imdb_id + ", overview=" + overview
				+ ", popularity=" + popularity + ", poster_path=" + poster_path + ", release_date=" + release_date
				+ ", revenue=" + revenue + ", runtime=" + runtime + ", status=" + status + ", tagline=" + tagline
				+ ", title=" + title + ", video=" + video + ", vote_average=" + vote_average + ", vote_count="
				+ vote_count + "]";
	}
	

}





// supports "genres" from MovieDetails
class Genres {
	
	private int id;
	private String name;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Override
	public String toString() {
		return "genres [id=" + id + ", name=" + name + "]";
	}
			
}





class ProductionCompany {
	
	private int id;
	private String logo_path;
	private String name;
	private String origin_company;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogo_path() {
		return logo_path;
	}
	public void setLogo_path(String logo_path) {
		this.logo_path = logo_path;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrigin_company() {
		return origin_company;
	}
	public void setOrigin_company(String origin_company) {
		this.origin_company = origin_company;
	}
	
	
	@Override
	public String toString() {
		return "ProductionCompany [id=" + id + ", logo_path=" + logo_path + ", name=" + name + ", origin_company="
				+ origin_company + "]";
	}
		
}



class ProductionCountry {
	
	private String iso_3166_1;
	private String name;
	
	
	public String getIso_3166_1() {
		return iso_3166_1;
	}
	public void setIso_3166_1(String iso_3166_1) {
		this.iso_3166_1 = iso_3166_1;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Override
	public String toString() {
		return "ProductionCountry [iso_3166_1=" + iso_3166_1 + ", name=" + name + "]";
	}
	
	
}




class SpokenLanguage {
	
	private String iso_639_1;
	private String name;
	
	
	public String getIso_639_1() {
		return iso_639_1;
	}
	public void setIso_639_1(String iso_639_1) {
		this.iso_639_1 = iso_639_1;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Override
	public String toString() {
		return "SpokenLanguage [iso_639_1=" + iso_639_1 + ", name=" + name + "]";
	}
	
	
}



class MovieCollection {
	
	private int id;
	private String name;
	private String poster_path;
	private String backdrop_path;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPoster_path() {
		return poster_path;
	}
	public void setPoster_path(String poster_path) {
		this.poster_path = poster_path;
	}
	public String getBackdrop_path() {
		return backdrop_path;
	}
	public void setBackdrop_path(String backdrop_path) {
		this.backdrop_path = backdrop_path;
	}
	
	
	@Override
	public String toString() {
		return "MovieCollection [id=" + id + ", name=" + name + ", poster_path=" + poster_path + ", backdrop_path="
				+ backdrop_path + "]";
	}
			
}


