package com.vadenent;

import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/")
public class ControllerRegister {
	
	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	UserDataRepository userDataRepository;
		
	
	// === Register a new user page ==========================================
	@GetMapping("/register")
	public String registerView(Model model, FormRegister registerForm) {
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("formRegister", registerForm);
		
		return "register";
	}

	
	@PostMapping("/register")
	public String registerUpdate(Model model, @Valid FormRegister registerForm, Errors errors, RedirectAttributes redirect) {
		
		model.addAttribute("loginStatus", loginStatus);

		String password = registerForm.getPassword1();
		
		validatePassword(password, errors);
		
		if (errors.hasErrors()) {
			
			return "register";
		}
		
		UserData newUser = new UserData();
		
		newUser.setUserName(registerForm.getUserName());
		newUser.setPassword(registerForm.getPassword1());
		
		UserData userLoginData = userDataRepository.findByUserName(newUser.getUserName());
		
		
		// null indicates the User is not already registered.  
		// !null indicates the User is already registered
		if (userLoginData != null) {	
					
			model.addAttribute("message", "This UserID " + newUser.getUserName() + " is already registered.");
			return "register";
			
		} else {
			
			
			// Not registered already and passwords match.  OK to register which is saving the userName and password
			if (registerForm.getPassword1().equals(registerForm.getPassword2())) {	
				
				String theSalt = PasswordValidator.generateSalt();
				newUser.setSalt(theSalt);
				
				// generate the hashed password and save userName and hashed password
				newUser.setPassword(PasswordValidator.hash(newUser.getPassword(), theSalt));		
				userDataRepository.save(newUser);
				
				redirect.addFlashAttribute("message", "You registered successfully as '" + registerForm.getUserName() + "'");
				return "redirect:/";
				
			} else {								// Not registered and passwords don't match
				
				model.addAttribute("message", "Your passwords did not match.  Try again.");
				return "register";
				
			} 

		}  
		
	}  // end of method registerUpdate
	
	
	// helper method to validate the password submitted when a user registers
	void validatePassword(String password, Errors errors) {
		
		boolean atLeastOneNumeric = Pattern.matches(".*[0-9].*", password);
		boolean atLeastOneAlphaUpper = Pattern.matches(".*[A-Z].*", password);
		boolean atLeastOneAlphaLower = Pattern.matches(".*[a-z].*", password);
		boolean containsNonAlphaNumeric = Pattern.matches(".*[^a-zA-Z0-9].*", password);
		
		int passwordLength = password.length();
		boolean passwordLengthIsValid = (passwordLength >= 8) && (passwordLength <= 20);
		
		// Length 8-20 inclusive; at least one each of: uppercase alpha, lowercase alpha, numeric
		if (	!atLeastOneNumeric 
			 || !atLeastOneAlphaUpper 
			 || !atLeastOneAlphaLower
			 || !passwordLengthIsValid
			 || containsNonAlphaNumeric) {
			
			errors.rejectValue("password1","oops", "Password must 8-20 alphanumeric only characters with at least 1 " 
								+ "numeric, 1 uppercase alpha and 1 lowercase alpha.");
		}
			
		return;
	}
	
}		// end of class RegisterController
