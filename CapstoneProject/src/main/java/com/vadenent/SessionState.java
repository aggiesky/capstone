package com.vadenent;

public class SessionState {

	private Long discussionViewDiscussionId;
	private Long homeViewDiscussionId;
	private Long homeViewFilterMovieId;
	private String selectedWatchgroupName;
	
	
	
	public String getSelectedWatchgroupName() {
		return selectedWatchgroupName;
	}
	public void setSelectedWatchgroupName(String selectedWatchgroupName) {
		this.selectedWatchgroupName = selectedWatchgroupName;
	}
	public Long getHomeViewFilterMovieId() {
		return homeViewFilterMovieId;
	}
	public void setHomeViewFilterMovieId(Long homeViewFilterMovieId) {
		this.homeViewFilterMovieId = homeViewFilterMovieId;
	}
	public Long getDiscussionViewDiscussionId() {
		return discussionViewDiscussionId;
	}
	public void setDiscussionViewDiscussionId(Long discussionViewDiscussionId) {
		this.discussionViewDiscussionId = discussionViewDiscussionId;
	}
	public Long getHomeViewDiscussionId() {
		return homeViewDiscussionId;
	}
	public void setHomeViewDiscussionId(Long homeViewDiscussionId) {
		this.homeViewDiscussionId = homeViewDiscussionId;
	}
	
	
	@Override
	public String toString() {
		return "SessionState [discussionViewDiscussionId=" + discussionViewDiscussionId + ", homeViewDiscussionId="
				+ homeViewDiscussionId + ", homeViewFilterMovieId=" + homeViewFilterMovieId
				+ ", selectedWatchgroupName=" + selectedWatchgroupName + "]";
	}

	
}
