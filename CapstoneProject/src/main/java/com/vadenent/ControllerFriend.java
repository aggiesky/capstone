package com.vadenent;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/")
public class ControllerFriend {
	
	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	UserDataRepository userDataRepository;
	
	@Autowired
	WatchgroupRepository watchgroupRepository;
	
	@Autowired
	SessionState sessionState;
	
	
	
	
	@GetMapping("/friends")
	public String getFriends(Model model) {
		
		// test: if user is not logged in, then return and do not show this page
		if (!loginStatus.isLoggedIn()) {
			return "redirect:/";
		}
		
		
		// Read the UserData objects from the database so we can make a list of usernames into "registeredUsers"
		List<UserData> theUserData = (List<UserData>) userDataRepository.findAll();

		List<String> registeredUsers = new ArrayList<>();

		for (UserData userData : theUserData) {
			registeredUsers.add(userData.getUserName());
		}
		
		// Now capture the loggedin user's friend names and put them in "formFriends"
		UserData tmpUserData = userDataRepository.findByUserName(loginStatus.getUserName());		
		FormFriends formFriends = new FormFriends();	
		List<String> friendNames = new ArrayList<>();
		
		for (Friend friend : tmpUserData.getFriends()) {
			friendNames.add(friend.getUsername());
		}
		
		formFriends.setListOfFriends(friendNames);
	
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("registeredUsers", registeredUsers);
		model.addAttribute("formFriends", formFriends);
		
		return "friends";
				
	}
	
	
	
	
	@PostMapping("/friends")
	public String postFriendsController(Model model, FormFriends formFriends, RedirectAttributes redirect) {
		
		
		model.addAttribute("loginStatus", loginStatus);

		UserData tmpUserData = userDataRepository.findByUserName(loginStatus.getUserName());
		List<Friend> friends = new ArrayList<>();
		
		for (String name : formFriends.getListOfFriends()) {
			Friend friend = new Friend();
			friend.setUsername(name);
			UserData userData = userDataRepository.findByUserName(name);
			friend.setUserId(userData.getId());
			friends.add(friend);
		}
		
		tmpUserData.setFriends(friends);
		
		userDataRepository.save(tmpUserData);
		
		redirect.addFlashAttribute("message", "You successfully updated your Friends List.\n");
		return "redirect:/";
		
	}

		
}
