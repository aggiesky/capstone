package com.vadenent;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
	
	public List<Comment> findByMovieIdAndWatchgroupIdOrderByLocationKeyAsc(Long ThreadId, Long WatchgroupId);
	public Comment 		 findFirstByLocationKey(String locationKey);
	public Comment 		 findFirstByLocationKeyAndDiscussionId(String locationKey, Long DiscussionId);
	public List<Comment> findByMovieIdAndDiscussionIdOrderByLocationKeyAsc(Long MovieId, Long DiscussionId);
	public List<Comment> findByDiscussionIdOrderByLocationKeyAsc(Long DiscussionId);
	
	public List<Comment> findByWatchgroupIdOrderByCommentDateTimeDesc(Long watchgroupId);
	public List<Comment> findByWatchgroupIdOrderByCommentDateTimeAsc(Long watchgroupId);
	public List<Comment> findByWatchgroupIdAndMovieIdOrderByCommentDateTimeDesc(Long watchgroupId, Long movieId);
	public List<Comment> findByWatchgroupIdAndMovieIdOrderByCommentDateTimeAsc(Long watchgroupId, Long movieId);
}
