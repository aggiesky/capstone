package com.vadenent;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class FormWatchgroup {
	
	@NotNull
	@Size(min=5, max=40)
	private String watchgroupName;
	private List<String> members;
	
	
	public String getWatchgroupName() {
		return watchgroupName;
	}
	public void setWatchgroupName(String watchgroupName) {
		this.watchgroupName = watchgroupName;
	}
	public List<String> getMembers() {
		return members;
	}
	public void setMembers(List<String> members) {
		this.members = members;
	}
	
	
	@Override
	public String toString() {
		return "FormWatchgroup [watchgroupName=" + watchgroupName + ", members=" + members + "]";
	}
	
	
}
