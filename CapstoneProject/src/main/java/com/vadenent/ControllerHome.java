package com.vadenent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;




@Controller
@RequestMapping("/")
public class ControllerHome {
	
	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	MovieRepository movieRepository;
	
	@Autowired
	DiscussionRepository discussionRepository;
	
	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	UserDataRepository userDataRepository;
	
	@Autowired
	SessionState sessionState;
	
	@Autowired
	WatchgroupRepository watchgroupRepository;
	

	final Long PUBLIC_WATCHGROUP = 1L;
	
	
	@GetMapping("/")
	public String showMain(Model model) {
		
		// Showing the Main page
		//
		// Main page always shows the Public watchgroup posts.  There can be one Public discussion for
		// each movie.  is only one Public "discussion"
		// which was initialized in the @PostConstruct in the CapstoneProjectApplication class.  This
		// discussion ID is retrieved from the SessionState and used to populate the Model for the 
		// Main page view.
		
		
		// get the discussionId, movieId and watchgroupId
		Long discussionId = sessionState.getHomeViewDiscussionId();

		Discussion homeViewDiscussion = new Discussion();
		Optional<Discussion> optionalDiscussion = discussionRepository.findById(discussionId);
		
		if (optionalDiscussion.isPresent()) 	homeViewDiscussion = optionalDiscussion.get();
	
		Long selectedMovieId = sessionState.getHomeViewFilterMovieId();
		
		
		//---------------------------------------------------------------------------------------------------
		// get All Discussions
		// if the user is not logged in, then only "Public" discussions are presented to the user.
		// if the user is logged in, then "Public" discussions and discussions in which the user is a member of 
		//    the discussion watchgroup are presented.
		List<Watchgroup> listOfWatchgroupObjects = new ArrayList<>();
		List<String> listOfWatchgroupNames = new ArrayList<>();
		
		listOfWatchgroupObjects = watchgroupRepository.findByMembers(loginStatus.getUserName());
		
		for (Watchgroup watchgroup : listOfWatchgroupObjects) {
			listOfWatchgroupNames.add(watchgroup.getWatchgroupName());
		}
		
		List<Discussion> allDiscussions = new ArrayList<>();
		List<Discussion> publicDiscussions = new ArrayList<>();
		
		if (loginStatus.isLoggedIn()) {
			publicDiscussions = (List<Discussion>) discussionRepository.findAllByPublicDiscussion(true);
			allDiscussions = (List<Discussion>) discussionRepository.findAllByWatchgroupNameIn(listOfWatchgroupNames);
			allDiscussions.addAll(publicDiscussions);
		}	/* else {
			publicDiscussions = (List<Discussion>) discussionRepository.findAllByPublicDiscussion(true);
			allDiscussions.addAll(publicDiscussions);
		}		*/
		
		// get all comments and scan each for the movieId and movieTitle
		// use a Set specifically to guarantee no duplicate entries
		// copy to List<> for adding to the Model
		Set<Movie> publicMovies = new TreeSet<>();
		List<Comment> comments = (List<Comment>) commentRepository.findAll();	// get all comments
		
		Movie movie0 = new Movie();
		movie0.setId(0L);
		movie0.setMovieTitle("All movies in Public discussions");
		publicMovies.add(movie0);
		
		for (Comment comment : comments) {
			Movie movie = new Movie();
			movie.setId(comment.getMovieId());
			movie.setMovieTitle(comment.getMovieTitle());
			publicMovies.add(movie);
		}
				
		// Load the model with view objects
		List<Movie> carouselMovies = (List<Movie>) movieRepository.findAll();

	
		FormPublicMovieFilter formPublicMovieFilter = new FormPublicMovieFilter();
		formPublicMovieFilter.setMovieId(sessionState.getHomeViewFilterMovieId());
		
		if (loginStatus.isLoggedIn()) {
			
			UserData userData = new UserData();
			Optional<UserData> optionalUserData = userDataRepository.findById(loginStatus.getUserId());
			if (optionalUserData.isPresent())  userData = optionalUserData.get();
			
			
			List<Friend> friends = userData.getFriends();
			for (Friend friend : friends) {				
				Optional<UserData> optionalTmpUserData = userDataRepository.findById(friend.getUserId());
				if (optionalTmpUserData.isPresent()) {
					UserData tmpUserData = optionalTmpUserData.get();
					friend.setOnLine(tmpUserData.isOnLine());
				}				
			}
						
			model.addAttribute("friends", friends);
			
		}
		
		
		if (!carouselMovies.isEmpty()) {

			// required for Navbar
			model.addAttribute("loginStatus", loginStatus);


			// for top left
			model.addAttribute("allDiscussions", allDiscussions);
			if (!loginStatus.isLoggedIn())		model.addAttribute("defaultOption", "Log in to Select a Discussion");
			else								model.addAttribute("defaultOption", "Select a Discussion");
			
			// for carousel
			model.addAttribute("movies", carouselMovies);
			
			// for top header for Comments section
			model.addAttribute("publicMovies", publicMovies);
			model.addAttribute("selectedMovieId", selectedMovieId);
			model.addAttribute("formPublicMovieFilter", formPublicMovieFilter);
	
			
		
			// for comments section; hard coded 1L for Public watchgroup on the main page
			// selectedMovieId=0 cause search to be done for all movies - no movie filtering
			// selectedMovieId!=0 causes search to filter using the movieId
			if (selectedMovieId == 0L) 				
				comments = (List<Comment>) commentRepository.findByWatchgroupIdOrderByCommentDateTimeDesc(PUBLIC_WATCHGROUP);
			else
				comments = (List<Comment>) commentRepository.findByWatchgroupIdAndMovieIdOrderByCommentDateTimeDesc(PUBLIC_WATCHGROUP, selectedMovieId);
			
			
			model.addAttribute("comments", comments);
			model.addAttribute("discussion", homeViewDiscussion);
			
			return "main";
			
		} else {

			System.out.println("What? No movie?");
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

	}
	
	
	
	
	
	@PostMapping("/filter")
	public String  putSetDiscussion(Model model, FormPublicMovieFilter formPublicMovieFilter) {
					
		sessionState.setHomeViewFilterMovieId(formPublicMovieFilter.getMovieId());
		
		return "redirect:/";
	}
	
	
}
