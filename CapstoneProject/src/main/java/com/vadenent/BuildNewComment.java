package com.vadenent;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class BuildNewComment {

	@Autowired
	LoginStatus loginStatus;
	
	
	//=============================================================================
	// helper method for the "/newComment" request handler
	public Comment buildNewComment(String locationKey,  
								   String commentTitle,
								   String commentText,
								   Discussion discussion) {
		
		//=========================================================================
		// Comment attributes:
		//
		// movieId:				retrieve from discussion object
		// movieTitle:			retrieve from discussion object
		// watchgroupId:		retrieve from discussion object
		// watchgroupLabel:		retrieve from discussion object
		// ownerId:				currently logged in user ID that is sourcing this request
		// ownerName:			currently logged in user name that is sourcing this request
		// commentTitle:		from newCommentFormField
		// commentText:			from newCommentFormField
		// locationKey:			calculated using the parent locationKey (parentKey) as the starting point.
		// commentDate:			returned from java.time.*
		// commentTime:			returned from java.time.*
		// discussionId         SessionState holds current discussionId
		// likes:				initialized here to 0
		// dislikes:			initialized here to 0
		// headslaps:			initialized here to 0
		
		Comment newComment = new Comment();
		
		newComment.setMovieId(discussion.getMovieId());
		newComment.setMovieTitle(discussion.getMovieName());
		newComment.setWatchgroupId(discussion.getWatchgroupId());
		newComment.setWatchgroupLabel(discussion.getWatchgroupName());
		
		newComment.setOwnerId(loginStatus.getUserId());			// hard coded for now
		newComment.setOwnerName(loginStatus.getUserName());		// hard coded for now
		newComment.setLikes(0);						// a new comment has all flags set to 0
		newComment.setDislikes(0);					//   "
		newComment.setHeadslaps(0);					//	 "
				
		LocalDateTime dateTime = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);   	// used for sorting the comments for display
		newComment.setCommentDateTime(dateTime.toString());
		
		newComment.setCommentTitle(commentTitle);		// Title returned from form post
		newComment.setCommentText(commentText); // Comment text returned from form post
		
		// Based on the locationKey of the parentComment, generate a locationKey for the new Comment
		
		HashMap<String, Integer> sortingTagMap = discussion.getSortingTagMap();
		
		SortingHat sortingHat = new SortingHat();
		
		newComment.setLocationKey(sortingHat.getNewSortingTag(locationKey, sortingTagMap));	
	
		
		// for calculating the number of indent levels: ignore 1st 7 characters; 
		//     4 characters (a ":999") is one indent level.
		newComment.setIndent(sortingHat.getIndentSize(locationKey));
		
		// set Discussion info. 
		newComment.setDiscussionId(discussion.getId());
		
		return newComment;
	}
	


}
