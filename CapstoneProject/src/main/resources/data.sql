insert into COMMENT
(ID, MOVIE_ID, MOVIE_TITLE, WATCHGROUP_ID, WATCHGROUP_LABEL, OWNER_ID, OWNER_NAME, COMMENT_TITLE, COMMENT_TEXT, LOCATION_KEY, COMMENT_DATE_TIME, DISCUSSION_ID, LIKES, DISLIKES, HEADSLAPS, INDENT)
values
(1, 1, 'True Grit', 2, 'Western Fans', 1, 'Jenn', 'Its a classic', 'Even the re-make was good.', 'discussion000002:999', '2019-08-27T13:45:00', 2, 2, 0, 0, 0),
(2, 1, 'True Grit', 2, 'Western Fans', 2, 'James', 'Scenery', 'This movie was shot on location in the San Juan mountains of southwest Colorado.', 'discussion000002:998', '2019-08-27T3:46:00', 2, 1, 0, 1, 0),
(3, 1, 'True Grit', 2, 'Western Fans', 2, 'James', 'Shoot out', 'The scene with John Wayne in the shoot out with Robert Duval is one of the classic shoot out scenes from movie history.', 'discussion000002:997', '2019-08-27T13:48:00', 2, 1, 0, 1, 0),
(4, 1, 'True Grit', 2, 'Western Fans', 2, 'James', 'Chin Le', 'Yes, Chin Le and the cat (whose name escapes me) are Rooster Cogburn''s family.', 'discussion000002:996', '2019-08-27T13:51:00', 2, 3, 1, 2, 0),
(5, 1, 'True Grit', 2, 'Western Fans', 1, 'Jenn', 'Texas Ranger', 'I didn''t think Glen Campbell was a very good actor, but I guess he got the job done.', 'discussion000002:995', '2019-08-27T13:55:00', 2, 1, 0, 1, 0),
(6, 1, 'True Grit', 2, 'Western Fans', 1, 'Jenn', 'The Ferry', 'Mattie was determined, to say the least, when she rode Little Blackie across the river at the ferry crossing.  Rooster was impressed.', 'discussion000002:994', '2019-08-27T13:55:32', 2, 1, 1, 0, 0),
(7, 1, 'True Grit', 1, 'Public', 1, 'Jenn', 'The snakes', 'Why was Mattie aggravating the rattlesnake?  She would have been fine if she didn''t keep poking at it.', 'discussion000001:999', '2019-09-20T14:26:00', 1, 0, 0, 0, 0),
(8, 1, 'True Grit', 1, 'Public', 2, 'James', 'One-eyed Fat Man', 'Classic answer to Rooster''s question. Guess he didn'' want to hang in Fort Smith.', 'discussion000001:998', '2019-09-20T14:27:00', 1, 0, 0, 0, 0),
(9, 2, 'Star Wars - A New Hope', 1, 'Public', 1, 'Jenn', 'Its a classic', 'Even the re-make was good.', 'discussion000003:999', '2019-08-27T13:45:00', 3, 2, 0, 0, 0),
(10, 2, 'Star Wars - A New Hope', 1, 'Public', 2, 'James', 'Scenery', 'This movie was shot on location in the San Juan mountains of southwest Colorado.', 'discussion000003:998', '2019-08-27T13:46:00', 3, 1, 0, 1, 0),
(11, 2, 'Star Wars - A New Hope', 1, 'Public', 2, 'James', 'Shoot out', 'The scene with John Wayne in the shoot out with Robert Duval is one of the classic shoot out scenes from movie history.', 'discussion000003:997', '2019-08-27T13:48:00', 3, 1, 0, 1, 0);



insert into MOVIE
(ID, MOVIE_TITLE, BACKDROP_IMAGE, POSTER_IMAGE, RELEASE_YEAR, MOVIE_ALT_TEXT, MOVIE_DESCRIPTION, GENRE)
values
(1, 'True Grit', 'images/TrueGrit1969-01.jpg', 'images/TrueGrit1969-01.jpg', '1969', 'Rooster Cogburn mounted on his horse, his pistol pulled and pointing at the Texas Ranger who will probably now start paying attention.', 'I think Rooster Cogburn is making his point.  Hmmm...  This doesn''t look like the Oklahoma Territory that I know.', 'western'),
(2, 'Star Wars - A New Hope', 'images/StarWars-01.jpg', 'images/StarWars-01.jpg', '1977', 'Luke, Leia and Hans all together escaping from the Death Star.', 'Still my favorite Star Wars movie.', 'sci-fi'),
(3, 'Casablanca', 'images/Casablanca-01.jpg', 'images/Casablanca-01.jpg', '1942', 'Picture of Sam playing the piano with Rick behind him - both in the cafe.', 'Probably my favorite ''old'' movie.  Of the 100 top movie quotes, 4 of the top 16 listed are from Casablanca.  I bet you can name a couple.', 'drama'),
(4, 'Nobody''s Fool', 'images/NobodysFool-01.jpg', 'images/NobodysFool-01.jpg', '1994', 'Poker at the ''Horse''.  Sully and Carl verbally sparring while the game goes on.', 'Paul Newman, Jessica Tandy and Bruce Willis - wait what?  How did Bruce Willis get in there?  I love the characters in this film.  No special effects, no Marvel, just some folks in Bath, New York.', 'drama');

