package com.vadenent;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/")
public class WatchgroupController {
	
	@Autowired
	LoginStatus loginStatus;
	
	@Autowired
	UserDataRepository userDataRepository;
	
	@Autowired
	WatchgroupRepository watchgroupRepository;
	
	
	
	@GetMapping("/watchgroup/new")
	public String wgNewController(Model model) {
	
		// Always need the loginStatus for the NavBar and possibly 
		// other features in the view template
		model.addAttribute("loginStatus", loginStatus);

		// Show the watchgroup view for creating a new watchgroup
		String viewFunction = "New";
		model.addAttribute("viewFunction", viewFunction);

		// Get the user's list of Friends & place in model		
		UserData userData = userDataRepository.findByUserName(loginStatus.getUserName());
		List<String> listOfFriends = userData.getFriends();		
		model.addAttribute("friends", listOfFriends);
		
		// Since we are creating a new watchgroup, place an empty Watchgroup form in the model
		FormWatchgroup formWatchgroup = new FormWatchgroup();
		model.addAttribute("formWatchgroup", formWatchgroup);
		
		return "watchgroupNew";
	}
	
	
	@PostMapping("/watchgroup/new")
	public String wgPostNewController(Model model, 
									  FormWatchgroup formWatchgroup, 
									  RedirectAttributes redirect) {
		
		// Capture the updated fields from the form
		Watchgroup group = new Watchgroup();
		group.setMembers(formWatchgroup.getMembers());
		group.setWatchgroupName(formWatchgroup.getWatchgroupName());
		group.setCreator(loginStatus.getUserName());
		
		// save the new watchgroup
		watchgroupRepository.save(group);
		
		redirect.addFlashAttribute("message", "You successfully create a new Watchgroup: "
											   + formWatchgroup.getWatchgroupName());
		
		return "redirect:/";
		
	}


	@GetMapping("/watchgroup/update")
	public String wgGetUpdate(Model model) {
	
		UserData userData = userDataRepository.findByUserName(loginStatus.getUserName());
		
		// Get the list of all watchgroups
		List<Watchgroup> groups = (List<Watchgroup>) watchgroupRepository.findAll();
		List<String> listOfWatchgroups = new ArrayList<>();
		
		listOfWatchgroups.add("Select a Watchgroup");
		for (Watchgroup group : groups ) {
			listOfWatchgroups.add(group.getWatchgroupName());
		}
		
		model.addAttribute("listOfWatchgroups", listOfWatchgroups);
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("friends", userData.getFriends());

		FormWatchgroup formWatchgroup = new FormWatchgroup();
		
		model.addAttribute("formWatchgroup", formWatchgroup);
							
		return "watchgroupEdit";
	}
	
	
	@PostMapping("/watchgroup/update")
	public String wgPostUpdateController(Model model, 
			@RequestParam(defaultValue = "inactive") String selectedWatchgroup) {
		
		if (! "inactive".equals(selectedWatchgroup)) {
			
			// remove trailing comma if it exists - seems to be appended by the binder
			if (selectedWatchgroup.endsWith(",")) {
				selectedWatchgroup = selectedWatchgroup.substring(0,selectedWatchgroup.lastIndexOf(","));
			}
			// Retrieve member list for the selected watchgroup
			// Save name and member list in the form
			Watchgroup watchgroup = watchgroupRepository.findByWatchgroupName(selectedWatchgroup);
			
			FormWatchgroup formWatchgroup = new FormWatchgroup();
			formWatchgroup.setWatchgroupName(selectedWatchgroup);
			formWatchgroup.setMembers(watchgroup.getMembers());
			
			// retrieve the list of friends for this user
			UserData userData = userDataRepository.findByUserName(loginStatus.getUserName());

			// Get the list of all watchgroups
			List<Watchgroup> groups = (List<Watchgroup>) watchgroupRepository.findAll();
			List<String> listOfWatchgroups = new ArrayList<>();
			
			listOfWatchgroups.add("Select a Watchgroup");
			for (Watchgroup group : groups ) {
				listOfWatchgroups.add(group.getWatchgroupName());
			}
			
			model.addAttribute("listOfWatchgroups", listOfWatchgroups);
			model.addAttribute("loginStatus", loginStatus);
			model.addAttribute("formWatchgroup", formWatchgroup);
			model.addAttribute("friends", userData.getFriends());
			
			return "watchgroupEdit";
		
		} else {	
			return "redirect:/watchgroup/update";
		}	
	}
		


	
	@GetMapping("/friends")
	public String getFriends(Model model) {
		
		List<String> registeredUsers = new ArrayList<>();
		
		registeredUsers.add("Jenn");
		registeredUsers.add("James");
		registeredUsers.add("Andy");
		registeredUsers.add("Liam");
		registeredUsers.add("Chris");
		registeredUsers.add("Lena");
		registeredUsers.add("Jon");
		registeredUsers.add("Glenne");
		registeredUsers.add("Theresa");
		registeredUsers.add("Charlie");
				
		
		UserData tmpUserData = userDataRepository.findByUserName(loginStatus.getUserName());
		
		FormFriends formFriends = new FormFriends();
		formFriends.setListOfFriends(tmpUserData.getFriends());
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("registeredUsers", registeredUsers);
		model.addAttribute("formFriends", formFriends);
		
		return "friends";
				
	}
	
	@PostMapping("/friends")
	public String postFriendsController(Model model, FormFriends formFriends, RedirectAttributes redirect) {
		
		
		model.addAttribute("loginStatus", loginStatus);
				
		UserData tmpUserData = userDataRepository.findByUserName(loginStatus.getUserName());		
		tmpUserData.setFriends(formFriends.getListOfFriends());
		userDataRepository.save(tmpUserData);
		
		redirect.addFlashAttribute("message", "You successfully updated your Friends List.\n");
		return "redirect:/";
		
	}

		
}
